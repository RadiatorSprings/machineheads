import datetime
import os

from bs4 import BeautifulSoup
from .lib.http_client import HttpClient


class SiteParser:
    _news_list = []
    _date = None

    def __init__(self, date: datetime):
        self._date = date
        self._client = HttpClient(os.environ['SITE_HOST'])

    def parse(self):
        soup = BeautifulSoup(self._client.get_content().content, "html.parser")
        cols = soup.find('div', {'class': 'container'}).find('div', {'class': 'row'}).find_all('div', {'class', 'col'})
        for col in cols:
            card_body = col.find('div', {'class': 'card'}).find('div', {'class': 'card-body'})
            if card_body is not None:
                title = card_body.find('h2', {'class': 'card-title'}).text
                text = card_body.find('p', {'class': 'card-text'}).text
                date = card_body.find('small', {'class': 'text-muted'}).text
                if title and text and date == self._date.strftime('%d.%m.%Y'):
                    self._news_list.append({
                        'title': title,
                        'text': text,
                        'date': date,
                    })
        self._news_list = sorted(self._news_list, key=lambda k: k["date"])
        self._client.save_result(self._news_list[:50])
