import json
import requests
import pika
import os


class HttpClient:
    def __init__(self, url: str, headers=None):
        self._url = url
        self._headers = headers
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=os.environ['MQ_HOST'],
                port=os.environ['MQ_PORT']
            )
        )
        self._channel = self._connection.channel()
        self.reply_queue = self._channel.queue_declare(queue=os.environ['MQ_QUEUE'], durable=True)
        self._channel.exchange_declare(exchange='news', exchange_type='direct')
        self._channel.queue_bind(exchange='news', queue=self.reply_queue.method.queue)

    def get_content(self):
        response = requests.get(self._url, self._headers)
        if response.status_code != 200:
            raise ('Запрос вернул код ответа -> ' + str(response.status_code))
        return response

    def save_result(self, payload: list):
        for i in payload:
            self._channel.basic_publish(exchange='news', routing_key=os.environ['MQ_QUEUE'], body=json.dumps(i))
        self._connection.close()

