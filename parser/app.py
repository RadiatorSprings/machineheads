from datetime import date

from src.site_parser import SiteParser

site_parser = SiteParser(date.today())

site_parser.parse()
