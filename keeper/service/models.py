from django.db import models


class News(models.Model):
    date = models.DateTimeField(null=False, blank=False)
    title = models.TextField(null=False, blank=False)
    text = models.TextField(null=False, blank=False)
