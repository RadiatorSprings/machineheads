import datetime
import json
import os

from django.core.management import BaseCommand
import pika
from service.models import News


class Command(BaseCommand):
    def handle(self, *args, **options):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=os.environ['MQ_HOST'],
                port=os.environ['MQ_PORT']
            )
        )
        channel = connection.channel()
        reply_queue = channel.queue_declare(queue=os.environ['MQ_QUEUE'], durable=True)
        channel.exchange_declare(exchange='news', exchange_type='direct')
        channel.queue_bind(exchange='news', queue=reply_queue.method.queue)

        def callback(ch, method, properties, body):
            body = json.loads(body)
            # Здесь должна быть проверка на уникальность новости
            # Эту проверку можно завязать на ее урл
            news = News(
                date=datetime.datetime.strptime(body['date'], "%d.%m.%Y").strftime("%Y-%m-%d"),
                title=body['title'],
                text=body['text']
            )
            try:
                news.save()
            except: # Тут конечно надо обарботать нужные исключения
                print('не смогли сохранить данные но залогировали')

        channel.basic_consume(callback, queue=os.environ['MQ_QUEUE'], no_ack=True)
        print('Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()